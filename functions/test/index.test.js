const app = require("../index.js");
const request = require("supertest");

describe("Route", () => {
  it("should expect 200", done => {
    request(app.skyrex)
      .get("/gpu")
      .expect(200, done);
  });
  it("should expect 200", done => {
    request(app.skyrex)
      .get("/name")
      .expect(200, done);
  });
  it("should expect 200", done => {
    request(app.skyrex)
      .get("/")
      .expect(200, done);
  });
});
