const functions = require("firebase-functions");

const express = require("express");

const app = express();

app.get("/gpu", (request, response) => {
  response.send("Nvidia has the best GPUs.");
});

app.get("*", (request, response) => {
  response.send("Hello! This is a small service endpoint created by Skyrex");
});

app.get("/name", (request, response) => {
  response.send("hello, this is mark");
});

const skyrex = functions.https.onRequest(app);

module.exports = {
  skyrex
};
